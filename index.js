const jsonRaw = require('./build/web/json/raw/tokens.json')
const jsonNested = require('./build/web/json/nested/tokens.json')
const jsonFlat = require('./build/web/json/flat/tokens.json')
const jsonCustomForDocs = require('./build/web/json/custom/tokens.json')

module.exports = {
    json: {
        raw: jsonRaw,
        forDocs: jsonCustomForDocs,
        nested: jsonNested,
        flat: jsonFlat,
    },
}  