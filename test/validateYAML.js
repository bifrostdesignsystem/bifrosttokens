const YamlValidator = require('yaml-validator');
const glob = require("glob");
const chalk = require('chalk');

// options is optional
glob("tokens/**/*.yaml", {}, function (er, files) {
    // files is an array of filenames.
    // If the `nonull` option is set, and nothing
    // was found, then files is ["**/*.js"]
    // er is an error object or null.
 
    // Default options
    const options = {
        log: true,
        structure: false,
        onWarning: function (error, filepath) {
                console.log(filepath + ' has error: ' + error)
            },
        writeJson: false
    };

    const validator = new YamlValidator(options);
    validator.validate(files);
   
    if(validator.report() === 1){
        throw new Error(chalk.redBright.bold("🔥🔥🔥💩🔥🔥🔥", validator.logs));
    } else {
        console.log(
            chalk.greenBright.bold(' ✅  All YAML files Tokens are Validated 🙌 !! ')
        );

    }
})


