# Bifstöst Tokens

Bifröst Tokens was created as a school project by Magnus V. Jensen


## Set up guide:
---

### Using npm:

 `npm i @bifrost-ds/bifrosttokens`

#### JavaScript

```javascript
// using import
import BT from '@bifrost-ds/bifrosttokens';

// vanilla
const BT = require('@bifrost-ds/bifrosttokens');

const JsonTokens = BT.flat;

```

### CSS

```css
@import url("../../node_modules/@bifrost-ds/bifrosttokens/build/web/css/variables.css");

  /* Use css variables */

  div{
    backgorund-color: var(--bt-color-secondary-green-a-100):
  }


```


### Tokens Export formats
---

- [CSS variable](https://bitbucket.org/bifrostdesignsystem/bifrosttokens/src/master/build/web/css/variables.css)
- [Json flat](https://bitbucket.org/bifrostdesignsystem/bifrosttokens/src/master/build/web/json/flat/tokens.json)
- [Json nested](https://bitbucket.org/bifrostdesignsystem/bifrosttokens/src/master/build/web/json/nested/tokens.json)
- [Json raw](https://bitbucket.org/bifrostdesignsystem/bifrosttokens/src/master/build/web/json/raw/tokens.json)
- [Json costum](https://bitbucket.org/bifrostdesignsystem/bifrosttokens/src/master/build/web/json/custom/tokens.json)



### Contribute / Feedback

Source Code:

- [Bifröst Tokens](https://bitbucket.org/bifrostdesignsystem/bifrosttokens/src/master/)
- [Bifröst Docs](https://bitbucket.org/bifrostdesignsystem/bifrostdesignsystemdocs/src/master/)

Docs:
 [Bifröst Website](https://goofy-stonebraker-aaabd2.netlify.app/)



 