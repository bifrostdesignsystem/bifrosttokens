const StyleDictionary = require('style-dictionary');
const yaml = require('yaml');
const fs = require('fs');
const _ = require('lodash');
const handlebars = require("handlebars");



const transformer = StyleDictionary.transform['attribute/cti'].transformer;

// ! Config for better structure of component tokens is taken from https://github.com/amzn/style-dictionary/tree/main/examples/advanced/component-cti

const propertiesToCTI = {
  'width': {category: 'size', type: 'dimension'},
  'min-width': {category: 'size', type: 'dimension'},
  'max-width': {category: 'size', type: 'dimension'},
  'height': {category: 'size', type: 'dimension'},
  'min-height': {category: 'size', type: 'dimension'},
  'max-height': {category: 'size', type: 'dimension'},
  'border-width': {category: 'size', type: 'border', item: 'width'},
  'border-radius': { category: 'size', type: 'border', item: 'width' },
  'border-color': {category: 'color', type: 'border'},
  'background-color': {category: 'color', type: 'background'},
  'color': {category: 'color', type: 'font'},
  'text-color': { category: 'color', type: 'font' },
  'padding': {category: 'size', type: 'padding'},
  'padding-vertical': {category: 'size', type: 'padding'},
  'padding-horizontal': {category: 'size', type: 'padding'},
  'icon': {category: 'content', type: 'icon'},
  'font-size': {category: 'size', type: 'font'},
  'line-height': { category: 'size', type: 'line-height' },
  'size': {category: 'size', type: 'icon'}
}

const CTITransform = {
  type: `attribute`,
  transformer: (prop) => {
    // Only do this custom functionality in the 'component' top-level namespace.
    if (prop.path[0] === 'component') {
      // When defining component tokens, the key of the token is the relevant CSS property
      // The key of the token is the last element in the path array
      return propertiesToCTI[prop.path[prop.path.length - 1]];
    } else {
      // Fallback to the original 'attribute/cti' transformer
      return transformer(prop);
    }
  }
}

// We can call .registerTransform here
// or apply the custom transform directly in the configuration below
//
// StyleDictionary.registerTransform({
//   name: 'attribute/cti',
//   type: 'attribute',
//   transformer: CTITransform.transformer
// });

// * Custom Templating insprired from link below, using handlebars
// * Link: https://github.com/amzn/style-dictionary/tree/main/examples/advanced/custom-formats-with-templates
const templateCustomJson = handlebars.compile(fs.readFileSync(__dirname + '/templates/web-json.template.hbs', 'utf8'));
StyleDictionary.registerFormat({
  name: 'custom/format/json',
  formatter: function({dictionary, platform}) {
    return templateCustomJson({
      // this is to show that the formatter function only takes a "dictionary" and "platform" parameters
      // (and dictionary has "tokens" and "allTokens" attributes)
      // and returns a string. for more details about the "formatter" function refer to the documentation
      allTokens: dictionary.allTokens,
      tokens: dictionary.tokens,
      options: platform
    });
  }
});


module.exports = {
  // ! Costum Parser to use YAML as token source instad of JSON, as documented here https://github.com/amzn/style-dictionary/tree/main/examples/advanced/yaml-tokens
  parsers: [{
    // A custom parser will only run against filenames that match the pattern
    // This pattern will match any file with the .yaml extension.
    // This allows you to mix different types of files in your token source
    pattern: /\.yaml$/,
    // the parse function takes a single argument, which is an object with
    // 2 attributes: contents which is a string of the file contents, and
    // filePath which is the path of the file.
    // The function is expected to return a plain object.
    parse: ({contents, filePath}) => yaml.parse(contents)
  }],
  // Rather than calling .registerTransform() we can apply the new transform
  // directly in our configuration. Using .registerTransform() with the same
  // transform name, 'attribute/cti', would work as well.
  transform: {
    // Override the attribute/cti transform
    'attribute/cti': CTITransform
  },
  source: [`tokens/**/*.yaml`],
  platforms: {
    css: {
      transforms: ["attribute/cti", "name/cti/kebab", "size/px", "color/css"],
      buildPath: 'build/web/css/',
      prefix: "bt",
      files: [{
        format: 'css/variables',
        destination: 'variables.css'
      }]
    },
    json: {
      transforms: ["attribute/cti", "name/cti/kebab", "size/px", "color/css"],
      buildPath: 'build/web/json/',
      prefix: "bt",
      files: [ 
        {
        format: "json",
        destination: "/raw/tokens.json"
      },
      {
        format: "json/nested",
        destination: "nested/tokens.json"
      },
      {
        format: "json/flat",
        destination: "flat/tokens.json"
      },
      {
        format: "custom/format/json",
        destination: "custom/tokens.json"
      }
    ]
    }
  }
}